# GISAID2ENA submission

ENA data submission from GISAID metadata

## Requirements
ENA webin-cli: https://github.com/enasequence/webin-cli/releases

## Usage
    ./gisaid2ena.py <configfile> <submissionfile>
An example submissionfile can be found in examples/submissionfile.yaml

## License
The source code is licensed under GPL-3.0-or-later and available via GitLab.
For more information or any inquiries, please reach out to legal@sib.swiss.

## Support
We have now moved this code to our SPSP Java backend (spsp.ch). This source code in python is therefore no longer maintained. You may contact spsp-support@sib.swiss in case of questions.
