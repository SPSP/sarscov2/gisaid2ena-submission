#!/usr/bin/env python3 
import datetime
import os
import re
import sys
import xml.etree.ElementTree as xmlTree

# import lxml.etree as xmlTree
import pandas
import yaml


class Context:
    is_dev = True

    def __init__(self, config, batch_folder, stage):
        self.config = config
        self.batch_folder = batch_folder
        self.input_folder = os.path.join(batch_folder, 'INPUT')
        self.output_folder = os.path.join(batch_folder, 'OUTPUT')
        Context.is_dev = stage != 'PROD'
        if Context.is_dev:
            self.url = 'https://wwwdev.ebi.ac.uk/ena/submit/drop-box/submit/'
        else:
            self.url = 'https://www.ebi.ac.uk/ena/submit/drop-box/submit/'

        clear_folder_content(self.input_folder)
        clear_folder_content(self.output_folder)


def is_not_blank(s):
    return bool(s and s.strip())


def initialize_config(configfile):
    with open(configfile, 'r') as file:
        info = yaml.load(file, yaml.Loader)
    return info


def load_metadata_gisaid(metadata_file, taxon_id, host):
    """
    Params: taxID, species_name and host_name are required
    ENA: https://www.ebi.ac.uk/ena/browser/view/ERC000033
    """
    df = pandas.read_excel(metadata_file, sheet_name='Submissions')
    df.drop([0], inplace=True)  # delete first row (header labels)
    df['taxon id'] = taxon_id
    #    df['species'] = species_name
    df['host scientific name'] = host
    df['host subject id'] = 'unknown'
    countries = []
    regions = []
    for loc in df['covv_location']:
        names = loc.split('/')
        countries.append(names[1].strip())
        regions.append(names[2].strip())
    df['geographic location (country and/or sea)'] = countries
    df['geographic location (region and locality)'] = regions
    df['collecting institution'] = df[['covv_orig_lab', 'covv_orig_lab_addr']].agg(','.join, axis=1)
    df.rename(columns={
        'covv_seq_technology': 'sequencing method',
        'covv_collection_date': 'collection date',
        'covv_host': 'host common name',
        'covv_authors': 'collector name',
        'covv_virus_name': 'virus name',
        'covv_specimen': 'isolation source host-associated',
        'covv_assembly_method': 'library_construction protocol',
        'covv_coverage': 'coverage',
        'covv_gender': 'host sex',
        'covv_patient_age': 'host age',
        'covv_patient_status': 'host health state',
        'fn': 'fasta file'
    }, inplace=True)
    return df


def submit_metadata(files, context):
    import requests
    response = requests.post(context.url, files=files, auth=(context.config['USER'], context.config['PWD']))
    if not response.ok:
        raise Exception('\nOops:\n' + response.text)
    receipt = response.content
    print(response.text)

    # write log in OUTPUT dir
    log_filename = os.path.join(context.output_folder, 'ENA_receipt.log')
    with open(log_filename, 'a+') as file:
        file.write(receipt.decode('utf-8'))
        file.write('\n{0}\n'.format('*' * 80))
    return receipt


def create_add_submission_xml(schema, alias, center_name, hold_date):
    return f"""<?xml version='1.0' encoding='UTF-8'?>
        <SUBMISSION alias="{alias}" center_name="{center_name}"> <ACTIONS>
            <ACTION> 
                <ADD schema="{schema}"/> 
            </ACTION>
            <ACTION> <HOLD HoldUntilDate = "{hold_date}"/> </ACTION >
        </ACTIONS> </SUBMISSION>"""


def submit_data(submission_type, data_xml, alias, center_name, context):
    date_str = datetime.date.today().strftime('%Y-%m-%d')
    submission_xml = create_add_submission_xml(submission_type.lower(), alias, center_name, date_str)
    files = {
        'SUBMISSION': ('.xml', submission_xml),
        submission_type: ('.xml', data_xml)
    }

    receipt = submit_metadata(files, context)
    return process_receipt(receipt, submission_type)


def submit_release(data_accession, alias, center_name, context):
    release_xml = create_release_xml(alias, center_name, data_accession)
    files = {
        'SUBMISSION': ('.xml', release_xml),
    }
    receipt = submit_metadata(files, context)
    receipt_root = xmlTree.fromstring(receipt)
    success = receipt_root.get('success')
    if success.lower() == 'true':
        return True

    error = receipt_root.find('MESSAGES/ERROR')
    if error.text.lower().find('only private') >= 0:
        return True  # "Failed to release ... Only private or cancelled objects may be released"
    raise Exception('\nOops:\n' + error.text)


def process_receipt(receipt, submission_type):
    receipt_root = xmlTree.fromstring(receipt)
    success = receipt_root.get('success')
    if success.lower() == 'true':
        elem = receipt_root.find(submission_type)
        return elem.get('accession'), elem.find('EXT_ID').get('accession')

    error = receipt_root.find('MESSAGES/ERROR')
    raise Exception('\nOops:\n' + error.text)


def create_project_description_xml(alias, center_name, title, description):
    return f"""<?xml version='1.0' encoding='UTF-8'?>
        <PROJECT_SET>
            <PROJECT alias="{alias}" center_name="{center_name}">
                <TITLE>{title}</TITLE>
                <DESCRIPTION>{description}</DESCRIPTION>
                <SUBMISSION_PROJECT>
                    <SEQUENCING_PROJECT/>
                </SUBMISSION_PROJECT>
            </PROJECT>
        </PROJECT_SET>
        """


def create_release_xml(alias, center_name, accession):
    return f"""<?xml version='1.0' encoding='UTF-8'?>
        <SUBMISSION alias="{alias}" center_name="{center_name}">
            <ACTIONS>
                <ACTION>
                    <RELEASE target="{accession}"/>
                </ACTION>
            </ACTIONS>
        </SUBMISSION>
        """


def create_sample_description_xml(m, sample_alias, center_name):
    def add(parent, tag, value):
        xmlTree.SubElement(parent, tag).text = value

    def handle_tag_value(v):
        return v if ((is_not_blank(v) and v != 'unknown') and (v != 'NaN')) else 'not provided'

    def add_attributes(tag, value):
        attribute = xmlTree.SubElement(sample_attributes, 'SAMPLE_ATTRIBUTE')
        add(attribute, 'TAG', tag)
        add(attribute, 'VALUE', handle_tag_value(value))

    sample = xmlTree.Element('SAMPLE', alias=sample_alias, center_name=center_name)

    add(sample, 'TITLE', m['virus name'])
    sample_name = xmlTree.SubElement(sample, 'SAMPLE_NAME')
    add(sample_name, 'TAXON_ID', str(m['taxon id']))
    # add(sample_name, 'SCIENTIFIC_NAME', m['species'])
    # add(sample_name, 'COMMON_NAME', '')
    sample_attributes = xmlTree.SubElement(sample, 'SAMPLE_ATTRIBUTES')
    add_attributes('ENA - CHECKLIST', 'ERC000033')
    add_attributes('collection date', m['collection date'])
    add_attributes('collecting institution', m['collecting institution'])
    add_attributes('collector name', m['collector name'])
    add_attributes('geographic location (country and/or sea)', m['geographic location (country and/or sea)'])
    add_attributes('geographic location (region and locality)', m['geographic location (region and locality)'])
    add_attributes('host common name', m['host common name'])
    add_attributes('host scientific name', m['host scientific name'])
    add_attributes('host subject id', m['host subject id'])
    add_attributes('host health state', m['host health state'])
    add_attributes('host sex', m['host sex'])
    add_attributes('isolate', m['virus name'])
    xml = xmlTree.tostring(sample, encoding='unicode')

    return xml


def split_multi_fasta(multi_fasta_name, context):
    """
    Extract multiple sequence fasta file and write each sequence in separate file.
    Modified from: https://www.biostars.org/p/340937/

    ! IMPORTANT !
    - We assume that we have complete genomes in multi-FASTA.
    - Since these are assumed to be complete genomes, we submit as chromosome => need to make it more general
    - Need to standardize also virus names (cf. seq_rec.id below)
    """

    def gzip_file(filepath):
        import gzip
        import shutil
        with open(filepath, 'rb') as f_in:
            with gzip.open(filepath + '.gz', 'wb') as f_out:
                shutil.copyfileobj(f_in, f_out)

    from Bio import SeqIO
    with open(os.path.join(context.batch_folder, multi_fasta_name)) as f_in:
        record = SeqIO.parse(f_in, "fasta")
        file_count = 0
        for seq_rec in record:
            file_count += 1
            virus_name = seq_rec.id.replace('/', '_')
            virus_path = os.path.join(context.input_folder, virus_name)
            fasta_fn = virus_path + '.fasta'
            chromosome_fn = virus_path + '_chr.txt'  # TODO: currently assumes there's only 1 segment, i.e. complete genome
            seq_rec.description = ''
            seq_rec.id = virus_name + '-' + str(file_count)
            with open(fasta_fn, 'w') as f_out:
                SeqIO.write(seq_rec, f_out, 'fasta')
            gzip_file(fasta_fn)

            with open(chromosome_fn, 'w') as f_out:
                f_out.write(f'{seq_rec.id}\t1\tLinear-Chromosome')
            gzip_file(chromosome_fn)

    if file_count == 0:
        raise Exception(f'No valid sequence in fasta file "{multi_fasta_name}"')
    return 'Done splitting multi-FASTA file and writing associated chromosome files.'


def write_manifest_file(project_alias, sample_acc, metadata_virus, context):
    virus_name = metadata_virus['virus name'].replace('/', '_')
    data = [
        ('STUDY', project_alias),
        ('SAMPLE', sample_acc),
        ('ASSEMBLYNAME', sample_acc),
        ('ASSEMBLY_TYPE', 'COVID-19 outbreak'),
        ('COVERAGE', metadata_virus['coverage']),
        ('PROGRAM', metadata_virus['library_construction protocol']),
        ('PLATFORM', metadata_virus['sequencing method']),
        ('MOLECULETYPE', 'genomic RNA'),
        ('FASTA', virus_name + '.fasta.gz'),
        ('CHROMOSOME_LIST', virus_name + '_chr.txt.gz')]

    text = ''
    for tag, value in data:
        text += f'{tag}\t{value}\n'
    manifest_filename = os.path.join(context.input_folder, virus_name) + '_MANIFEST.txt'
    with open(manifest_filename, "w") as file:
        file.write(text)
    return manifest_filename


def run_cmd(cmd_line):
    """Execute command line.
    :param cmd_line: the string of command line
    :return output: the string of output from execution
    From: https://github.com/usegalaxy-eu/ena-upload-cli/blob/master/ena_upload/ena_upload.py
    """
    import shlex
    import subprocess
    args = shlex.split(cmd_line)
    process = subprocess.Popen(args, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    output, stderr = process.communicate()
    return output


def submit_data_webin_cli(manifest_filename, center_name, context):
    cmd = 'java -jar {0} -context genome -userName "{1}" -password "{2}"' \
          ' -centerName "{3}" -manifest "{4}" -inputDir "{5}" -outputDir "{6}" -submit' \
        .format(context.config['WEBIN_CLI_PATH'], context.config['USER'], context.config['PWD'],
                center_name, manifest_filename, context.input_folder, context.output_folder)
    if Context.is_dev:
        cmd += ' -test'
    print(cmd)
    receipt = run_cmd(cmd).decode()
    print(receipt)
    message = receipt.lower()
    if message.find('completed successfully') >= 0 or message.find('already exists') >= 0:
        data_id = re.compile('[EDS]RZ[0-9]{6,}').findall(receipt)
        if len(data_id) == 1:
            return data_id[0]
    raise Exception('\nOops:\n' + receipt)


def clear_folder_content(folder):
    import os
    from shutil import rmtree
    if os.path.isdir(folder):
        rmtree(folder)
    os.makedirs(folder)


def main():
    """
    Usage: ./gisaid2ena.py <configfile> <submissionfile>
    """
    if len(sys.argv) < 3:
        raise Exception("Usage: ./gisaid2ena.py <config file> <submission file>")

    config_file = sys.argv[1]  # config yaml
    submission_file = sys.argv[2]  # submission yaml

    # (0) Initialize basic info like credentials and folders
    config = initialize_config(config_file)  # load yaml
    with open(submission_file, 'r') as file:
        submission_param = yaml.load(file, yaml.Loader)

    context = Context(config, submission_param['batch_folder'], submission_param['stage'])

    metadata = load_metadata_gisaid(
        os.path.join(context.batch_folder, submission_param['metadata_file']),
        submission_param['TAXID'], submission_param['HOST'])

    center_name = submission_param['center_name']
    study_accession = submission_param['project_accession']
    study_alias = submission_param['alias']

    results = []
    if not is_not_blank(study_accession):  # if project does not exist yet, register it
        description_xml = create_project_description_xml(study_alias, center_name,
                                                         submission_param['title'], submission_param['description'])
        study_accession = submit_data('PROJECT', description_xml, study_alias, center_name, context)[0]
        submit_release(study_accession, study_alias, center_name, context)
        results.append({
            'data_id': study_alias,
            'ena_project_id': study_accession
        })

        submission_param['project_accession'] = study_accession
        with open(submission_file, 'w') as file:
            yaml.dump(submission_param, file, sort_keys=False)

    split_multi_fasta(submission_param['fasta_file'], context)

    # Register samples and submit assemblies
    for i, m in metadata.iterrows():
        sample_alias = m['virus name']
        if Context.is_dev:  # add the date, otherwise if alias already exists it will be refused
            sample_alias += ' ' + datetime.datetime.now().isoformat()

        sample_xml = create_sample_description_xml(m, sample_alias, center_name)
        (sample_accession, insdc_sample_id) = submit_data('SAMPLE', sample_xml, sample_alias, center_name, context)
        submit_release(sample_accession, sample_alias, center_name, context)

        manifest_filename = write_manifest_file(study_alias, sample_accession, m, context)
        ena_analysis_id = submit_data_webin_cli(manifest_filename, center_name, context)
        results.append({
            'data_id': sample_alias,
            'ena_sample_id': sample_accession,
            'ena_analysis_id': ena_analysis_id,
            'insdc_sample_id': insdc_sample_id
        })

        print('\n\nDONE. Please refer to the OUTPUT/ folder in batch_folder for ENA receipts and other logs.')

        import json
        results_json = json.dumps(results, indent=2)
        print(results_json)


if __name__ == "__main__":
    main()
